unit UnFrmBuscarHeranca;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, FireDAC.Stan.StorageXML,
  Datasnap.DBClient, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFrmBuscarHeranca = class(TForm)
    Panel1: TPanel;
    btnBuscar: TButton;
    btnSair: TButton;
    Panel2: TPanel;
    dbGrid: TDBGrid;
    cdsGrid: TClientDataSet;
    dsGrid: TDataSource;
    FDStanStorageXMLLink: TFDStanStorageXMLLink;
    procedure btnSairClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
  private
    FcURL_Request: String;
  public
    property cURL_Request: String read FcURL_Request write FcURL_Request;
  end;

var
  FrmBuscarHeranca: TFrmBuscarHeranca;

implementation

uses
  UnInterfaces, UnIGetDadosComToken;

{$R *.dfm}

procedure TFrmBuscarHeranca.btnBuscarClick(Sender: TObject);
begin
  cdsGrid.Close;
  cdsGrid.Data := TGetDadosComToken.New(FcURL_Request).Executar.SQLData;
  cdsGrid.Open;
end;

procedure TFrmBuscarHeranca.btnSairClick(Sender: TObject);
begin
  Self.Close;
end;

end.
