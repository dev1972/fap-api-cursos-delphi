object FrmBuscarHeranca: TFrmBuscarHeranca
  Left = 0
  Top = 0
  Caption = 'FrmBuscarHeranca'
  ClientHeight = 411
  ClientWidth = 467
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 467
    Height = 57
    Align = alTop
    TabOrder = 0
    object btnBuscar: TButton
      Left = 1
      Top = 1
      Width = 107
      Height = 55
      Align = alLeft
      Caption = 'Buscar'
      TabOrder = 0
      OnClick = btnBuscarClick
    end
    object btnSair: TButton
      Left = 359
      Top = 1
      Width = 107
      Height = 55
      Align = alRight
      Caption = 'Sair'
      TabOrder = 1
      OnClick = btnSairClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 57
    Width = 467
    Height = 354
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object dbGrid: TDBGrid
      Left = 0
      Top = 0
      Width = 467
      Height = 354
      Align = alClient
      DataSource = dsGrid
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
    end
  end
  object cdsGrid: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 24
    Top = 337
  end
  object dsGrid: TDataSource
    AutoEdit = False
    DataSet = cdsGrid
    Left = 72
    Top = 337
  end
  object FDStanStorageXMLLink: TFDStanStorageXMLLink
    Left = 152
    Top = 337
  end
end
