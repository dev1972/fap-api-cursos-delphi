unit UnFrmCadastroHeranca;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFrmCadastroHeranca = class(TForm)
    Panel1: TPanel;
    btnSalvar: TButton;
    btnSair: TButton;
    Panel2: TPanel;
    procedure btnSalvarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
  private
    FcURL_Request: String;
  protected
    function MontaJSON: String; virtual; abstract;
  public
    property cURL_Request: String read FcURL_Request write FcURL_Request;
  end;

var
  FrmCadastroHeranca: TFrmCadastroHeranca;

implementation

uses
  UnInterfaces, UnIPostDadosComToken;

{$R *.dfm}

procedure TFrmCadastroHeranca.btnSairClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TFrmCadastroHeranca.btnSalvarClick(Sender: TObject);
begin
  try
    TPostDadosComToken
      .New(FcURL_Request, MontaJSON)
      .Executar;

    ShowMessage('Cadastrado com Sucesso!');
  except
    on E: Exception do
    begin
      raise Exception.Create(E.Message);
    end;
  end;
end;

end.
