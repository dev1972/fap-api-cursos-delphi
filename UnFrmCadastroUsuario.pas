unit UnFrmCadastroUsuario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.DBCtrls, Vcl.Mask,
  Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, UnDMConexao;

type
  TFrmCadastroUsuario = class(TForm)
    DBNavigator1: TDBNavigator;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    lbUsuario: TLabel;
    Label1: TLabel;
    DBComboBox1: TDBComboBox;
    Label2: TLabel;
    qUsuario: TFDQuery;
    dsUsuario: TDataSource;
    qUsuarioid: TIntegerField;
    qUsuariologin: TWideStringField;
    qUsuariosenha: TWideStringField;
    qUsuariorole: TWideStringField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCadastroUsuario: TFrmCadastroUsuario;

implementation

{$R *.dfm}

procedure TFrmCadastroUsuario.FormShow(Sender: TObject);
begin
  qUsuario.Close;
  qUsuario.Open;
end;

end.
