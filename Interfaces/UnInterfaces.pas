unit UnInterfaces;

interface

type

  IRetornaToken = interface
    ['{1E5DFAB2-03D4-4DD2-BC02-085DE3388DEE}']

    function GetToken: string;
    procedure SetToken(AcToken: String);

    property Token: String read GetToken write SetToken;

    function Executar: IRetornaToken;
  end;

  IGetDados = interface
    ['{6E0E5A21-F79C-4CAE-8786-86BA8C485C04}']
    function GetSQLData: OleVariant;
    procedure SetSQLData(AvSQLData: OleVariant);

    property SQLData: OleVariant read GetSQLData write SetSQLData;

    function Executar: IGetDados;
  end;

  IPostDados = interface
    ['{0ACEB248-AE09-4ADA-9E6F-761234D1725C}']
    function Executar: IPostDados;
  end;

implementation

end.
