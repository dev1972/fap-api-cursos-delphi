unit UnIPostDadosComToken;

interface

uses
  UnInterfaces, FireDAC.Comp.Client, Data.DB, UnDmConexao, System.SysUtils, FireDAC.Stan.Param, DateUtils,
  Classes, Vcl.Dialogs, REST.Types, REST.Client, REST.Authenticator.OAuth, REST.Response.Adapter,
  Datasnap.DBClient;

type
  TPostDadosComToken = class(TInterfacedObject, IPostDados)
  private
    FcURL: String;
    FcJSON: string;
    FRESTClient: TRESTClient;
    FRESTRequest: TRESTRequest;
    FRESTResponse: TRESTResponse;
    FOAuth2Auth: TOAuth2Authenticator;
    procedure ConfiguraRestClient;
    procedure ConfiguraRestRequest;
    procedure ConfiguraRestResponse;
    procedure ConfiguraAuthenticator;
    procedure ExecutaRequest;
  public
    function Executar: IPostDados;
    class function New(AcURL, AcJSON: String): IPostDados;
    constructor Create(AcURL, AcJSON: String);
    destructor Destroy; override;
  end;

implementation

uses
  UnIRetornaTokenLogin;

const
  ACCEPT         = 'application/json, text/plain; q=0.9, text/html;q=0.8,';
  ACCEPT_CHARSET = 'utf-8, *;q=0.8';
  CONTENTTYPE    = 'application/json';

{ TPostDadosComToken }

class function TPostDadosComToken.New(AcURL, AcJSON: String): IPostDados;
begin
  Result := Self.Create(AcURL, AcJSON);
end;

constructor TPostDadosComToken.Create(AcURL, AcJSON: String);
begin
  FcURL         := AcURL;
  FcJSON        := AcJSON;
  FRESTClient   := TRESTClient.Create(nil);
  FRESTRequest  := TRESTRequest.Create(nil);
  FRESTResponse := TRESTResponse.Create(nil);
  FOAuth2Auth   := TOAuth2Authenticator.Create(nil);
end;

function TPostDadosComToken.Executar: IPostDados;
begin
  Result := Self;
  Self.ConfiguraRestClient;
  Self.ConfiguraRestRequest;
  Self.ConfiguraRestResponse;
  Self.ConfiguraAuthenticator;
  Self.ExecutaRequest;
end;

procedure TPostDadosComToken.ConfiguraRestClient;
begin
  FRESTClient.Accept        := ACCEPT;
  FRESTClient.AcceptCharset := ACCEPT_CHARSET;
  FRESTClient.BaseURL       := FcURL;
  FRESTClient.ContentType   := CONTENTTYPE;
  FRESTClient.Authenticator := FOAuth2Auth;
end;

procedure TPostDadosComToken.ConfiguraRestRequest;
begin
  FRESTRequest.Client   := FRESTClient;
  FRESTRequest.Method   := rmPOST;
  with FRESTRequest.Params.AddItem do
  begin
    ContentType := ctAPPLICATION_JSON;
    Name        := 'body';
    Kind        := pkREQUESTBODY;
  end;
  FRESTRequest.Response := FRESTResponse;
end;

procedure TPostDadosComToken.ConfiguraRestResponse;
begin
  FRESTResponse.ContentType := CONTENTTYPE;
end;

procedure TPostDadosComToken.ConfiguraAuthenticator;
begin
  FOAuth2Auth.TokenType   := TOAuth2TokenType.ttBEARER;
  FOAuth2Auth.AccessToken := TRetornaTokenLogin.New.Executar.Token;
end;

procedure TPostDadosComToken.ExecutaRequest;
begin
  FRESTRequest.Params.Items[0].Value := FcJSON;
  try
    FRESTRequest.Execute;
  except
    on E: Exception do
    begin
      ShowMessage(E.Message);
      Abort;
    end;
  end;
end;

destructor TPostDadosComToken.Destroy;
begin
  if Assigned(FRESTClient) then
  begin
    FRESTClient.Free;
  end;

  if Assigned(FRESTRequest) then
  begin
    FRESTRequest.Free;
  end;

  if Assigned(FRESTResponse) then
  begin
    FRESTResponse.Free;
  end;

  if Assigned(FOAuth2Auth) then
  begin
    FOAuth2Auth.Free;
  end;

  inherited;
end;

end.
