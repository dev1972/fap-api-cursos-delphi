unit UnIGetDadosComToken;

interface

uses
  UnInterfaces, FireDAC.Comp.Client, Data.DB, UnDmConexao, System.SysUtils, FireDAC.Stan.Param, DateUtils,
  Classes, Vcl.Dialogs, REST.Types, REST.Client, REST.Authenticator.OAuth, REST.Response.Adapter,
  Datasnap.DBClient;

type
  TGetDadosComToken = class(TInterfacedObject, IGetDados)
    private
      FcURL: String;
      FSQLData: OleVariant;
      FRESTClient: TRESTClient;
      FRESTRequest: TRESTRequest;
      FRESTResponse: TRESTResponse;
      FOAuth2Auth: TOAuth2Authenticator;
      FRESTRespDataSetAdapter: TRESTResponseDataSetAdapter;
      FCds: TClientDataSet;
      FDataSource: TDataSource;
      procedure SetSQLData(AvSQLData: OleVariant);
      procedure ConfiguraRestClient;
      procedure ConfiguraRestRequest;
      procedure ConfiguraRestResponse;
      procedure ConfiguraAuthenticator;
      procedure ConfiguraDataSetAdapter;
      procedure ConfiguraDataSource;
      procedure ExecutaRequest;
    public
      function GetSQLData: OleVariant;
      function Executar: IGetDados;
      class function New(AcURL: String): IGetDados;
      constructor Create(AcURL: String);
      destructor Destroy; override;
  end;

implementation

uses
  UnIRetornaTokenLogin;

const
  ACCEPT         = 'application/json, text/plain; q=0.9, text/html;q=0.8,';
  ACCEPT_CHARSET = 'utf-8, *;q=0.8';
  CONTENTTYPE    = 'application/json';

{ TGetDadosComToken }

class function TGetDadosComToken.New(AcURL: String): IGetDados;
begin
  Result := Self.Create(AcURL);
end;


constructor TGetDadosComToken.Create(AcURL: String);
begin
  FcURL                   := AcURL;
  FRESTClient             := TRESTClient.Create(nil);
  FRESTRequest            := TRESTRequest.Create(nil);
  FRESTResponse           := TRESTResponse.Create(nil);
  FOAuth2Auth             := TOAuth2Authenticator.Create(nil);
  FRESTRespDataSetAdapter := TRESTResponseDataSetAdapter.Create(nil);
  FDataSource             := TDataSource.Create(nil);
  FCds                    := TClientDataSet.Create(nil);
end;

function TGetDadosComToken.Executar: IGetDados;
begin
  Result := Self;
  Self.ConfiguraRestClient;
  Self.ConfiguraRestRequest;
  Self.ConfiguraRestResponse;
  Self.ConfiguraAuthenticator;
  Self.ConfiguraDataSetAdapter;
  Self.ConfiguraDataSource;
  Self.ExecutaRequest;
end;

procedure TGetDadosComToken.ExecutaRequest;
begin
  try
    FRESTRequest.Execute;
    FCds.Open;
  except
    on E: Exception do
    begin
      ShowMessage(E.Message);
      Abort;
    end;
  end;

  if not FCds.IsEmpty then
  begin
    Self.SetSQLData(FCds.Data)
  end;
end;

procedure TGetDadosComToken.ConfiguraRestClient;
begin
  FRESTClient.Accept        := ACCEPT;
  FRESTClient.AcceptCharset := ACCEPT_CHARSET;
  FRESTClient.BaseURL       := FcURL;
  FRESTClient.ContentType   := CONTENTTYPE;
  FRESTClient.Authenticator := FOAuth2Auth;
end;

procedure TGetDadosComToken.ConfiguraRestRequest;
begin
  FRESTRequest.Client   := FRESTClient;
  FRESTRequest.Method   := rmGET;
  FRESTRequest.Response := FRESTResponse;
end;

procedure TGetDadosComToken.ConfiguraRestResponse;
begin
  FRESTResponse.ContentType := CONTENTTYPE;
end;

procedure TGetDadosComToken.ConfiguraAuthenticator;
begin
  FOAuth2Auth.TokenType   := TOAuth2TokenType.ttBEARER;
  FOAuth2Auth.AccessToken := TRetornaTokenLogin.New.Executar.Token;
end;

procedure TGetDadosComToken.ConfiguraDataSetAdapter;
begin
  FRESTRespDataSetAdapter.Response := FRESTResponse;
  FRESTRespDataSetAdapter.Dataset  := FCds;
end;

procedure TGetDadosComToken.ConfiguraDataSource;
begin
  FDataSource.DataSet := FCds;
end;

function TGetDadosComToken.GetSQLData: OleVariant;
begin
  Result := FSQLData;
end;

procedure TGetDadosComToken.SetSQLData(AvSQLData: OleVariant);
begin
  FSQLData := AvSQLData;
end;

destructor TGetDadosComToken.Destroy;
begin
  if Assigned(FRESTClient) then
  begin
    FRESTClient.Free;
  end;

  if Assigned(FRESTRequest) then
  begin
    FRESTRequest.Free;
  end;

  if Assigned(FRESTResponse) then
  begin
    FRESTResponse.Free;
  end;

  if Assigned(FOAuth2Auth) then
  begin
    FOAuth2Auth.Free;
  end;

  if Assigned(FRESTRespDataSetAdapter) then
  begin
    FRESTRespDataSetAdapter.Free;
  end;

  if Assigned(FDataSource) then
  begin
    FDataSource.Free;
  end;

  if Assigned(FCds) then
  begin
    FCds.Free;
  end;

  inherited;
end;

end.
