unit UnIRetornaTokenLogin;

interface

uses
  UnInterfaces, FireDAC.Comp.Client, Data.DB, UnDmConexao, System.SysUtils, FireDAC.Stan.Param, DateUtils,
  Classes, Vcl.Dialogs, System.JSON, REST.Types, REST.Client, REST.Authenticator.OAuth, REST.Response.Adapter;

type
  TRetornaTokenLogin = class(TInterfacedObject, IRetornaToken)
    private
      FRESTClient: TRESTClient;
      FRESTRequest: TRESTRequest;
      FRESTResponse: TRESTResponse;
      FcJSONObject: TJSONObject;
      FcToken: String;
      procedure SetToken(AcToken: String);
      procedure ConfiguraRestClient;
      procedure ConfiguraRestRequest;
      procedure ConfiguraRestResponse;
      procedure ExecutaRequest;
      procedure MontaJson;
    public
      function GetToken: string;
      function Executar: IRetornaToken;
      class function New: IRetornaToken;
      constructor Create;
      destructor Destroy; override;
  end;

implementation

const
  ACCEPT         = 'application/json, text/plain; q=0.9, text/html;q=0.8,';
  ACCEPT_CHARSET = 'utf-8, *;q=0.8';
  URL_LOGIN      = 'https://localhost:5001/api/Login';
  CONTENTTYPE    = 'application/json';
  CODE_OK        = 200;

{ TRetornaTokenLogin }

class function TRetornaTokenLogin.New: IRetornaToken;
begin
  Result := Self.Create;
end;

constructor TRetornaTokenLogin.Create;
begin
  FcToken                 := EmptyStr;
  FcJSONObject            := TJSONObject.Create;
  FRESTClient             := TRESTClient.Create(nil);
  FRESTRequest            := TRESTRequest.Create(nil);
  FRESTResponse           := TRESTResponse.Create(nil);
end;

function TRetornaTokenLogin.Executar: IRetornaToken;
begin
  Result := Self;
  Self.ConfiguraRestClient;
  Self.ConfiguraRestRequest;
  Self.ConfiguraRestResponse;
  Self.MontaJson;
  Self.ExecutaRequest;
end;

procedure TRetornaTokenLogin.ExecutaRequest;
begin
  FRESTRequest.Params.Items[0].Value := FcJSONObject.ToString;
  try
    FRESTRequest.Execute;
  except
    on E: Exception do
    begin
      ShowMessage(E.Message);
      Abort;
    end;
  end;

  Self.SetToken(StringReplace(FRESTResponse.JSONValue.FindValue('token').ToString, '"', '', [rfReplaceAll, rfIgnoreCase]));
end;

procedure TRetornaTokenLogin.ConfiguraRestClient;
begin
  FRESTClient.Accept        := ACCEPT;
  FRESTClient.AcceptCharset := ACCEPT_CHARSET;
  FRESTClient.BaseURL       := URL_LOGIN;
  FRESTClient.ContentType   := CONTENTTYPE;
end;

procedure TRetornaTokenLogin.ConfiguraRestRequest;
begin
  FRESTRequest.Client := FRESTClient;
  FRESTRequest.Method := rmPOST;
  with FRESTRequest.Params.AddItem do
  begin
    ContentType := ctAPPLICATION_JSON;
    Name        := 'body';
    Kind        := pkREQUESTBODY;
  end;
  FRESTRequest.Response := FRESTResponse;
end;

procedure TRetornaTokenLogin.ConfiguraRestResponse;
begin
  FRESTResponse.ContentType := CONTENTTYPE;
end;

procedure TRetornaTokenLogin.MontaJson;
begin
  FcJSONObject.AddPair('id', TJSONNumber.Create(0));
  FcJSONObject.AddPair('login', DMConexao.qLogin.FieldByName('LOGIN').AsString);
  FcJSONObject.AddPair('senha', DMConexao.qLogin.FieldByName('SENHA').AsString);
  FcJSONObject.AddPair('role', 'role');
end;

function TRetornaTokenLogin.GetToken: string;
begin
  Result := FcToken;
end;

procedure TRetornaTokenLogin.SetToken(AcToken: String);
begin
  FcToken := AcToken;
end;

destructor TRetornaTokenLogin.Destroy;
begin
  if Assigned(FcJSONObject) then
  begin
    FcJSONObject.Free;
  end;

  if Assigned(FRESTClient) then
  begin
    FRESTClient.Free;
  end;

  if Assigned(FRESTRequest) then
  begin
    FRESTRequest.Free;
  end;

  if Assigned(FRESTResponse) then
  begin
    FRESTResponse.Free;
  end;

  inherited;
end;

end.
