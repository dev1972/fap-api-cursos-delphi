unit UnFrmBuscarDisciplina;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UnFrmBuscarHeranca, Data.DB,
  FireDAC.Stan.StorageXML, Datasnap.DBClient, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFrmBuscarDisciplina = class(TFrmBuscarHeranca)
    cdsGridID: TWideStringField;
    cdsGridNOMEDISCIPLINA: TWideStringField;
    cdsGridHORASAULA: TWideStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmBuscarDisciplina: TFrmBuscarDisciplina;

implementation

{$R *.dfm}

end.
