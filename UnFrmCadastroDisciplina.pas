unit UnFrmCadastroDisciplina;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UnFrmCadastroHeranca, Vcl.StdCtrls,
  Vcl.ExtCtrls, System.JSON;

type
  TFrmCadastroDisciplina = class(TFrmCadastroHeranca)
    edtID: TEdit;
    edtNomeDisciplina: TEdit;
    lbNomeDisciplina: TLabel;
    lbID: TLabel;
    lbHorasAula: TLabel;
    edtHorasAula: TEdit;
    procedure btnSalvarClick(Sender: TObject);
  private
    { Private declarations }
  protected
    function MontaJSON: String; override;
  public
    { Public declarations }
  end;

var
  FrmCadastroDisciplina: TFrmCadastroDisciplina;

implementation

{$R *.dfm}

{ TFrmCadastroDisciplina }

procedure TFrmCadastroDisciplina.btnSalvarClick(Sender: TObject);
begin
  if StrToIntDef(edtID.Text, 0) <= 0 then
  begin
    ShowMessage('C�digo Inv�lido... Verifique!')
  end;

  if Trim(edtNomeDisciplina.Text) = EmptyStr then
  begin
    ShowMessage('Nome Inv�lido... Verifique!')
  end;

  if StrToIntDef(edtHorasAula.Text, 0) < 0 then
  begin
    ShowMessage('C�digo Inv�lido... Verifique!')
  end;

  inherited;

  Self.Close;
end;

function TFrmCadastroDisciplina.MontaJSON: String;
var
  FcJSONObject: TJSONObject;
begin
  inherited;
  Result        := EmptyStr;
  FcJSONObject  := TJSONObject.Create;
  try
    FcJSONObject.AddPair('id', TJSONNumber.Create(StrToInt(edtID.Text)));
    FcJSONObject.AddPair('nomeDisciplina', edtNomeDisciplina.Text);
    FcJSONObject.AddPair('horasAula', TJSONNumber.Create(StrToIntDef(edtHorasAula.Text, 0)));
    Result := FcJSONObject.ToString;
  finally
    FcJSONObject.Free;
  end;
end;

end.
