program APIDelphiConsumer;

uses
  Vcl.Forms,
  UnFrmMenuPrincipal in 'UnFrmMenuPrincipal.pas' {FrmMenuPrincipal},
  UnFrmLogin in 'UnFrmLogin.pas' {FrmLogin},
  UnUtils in 'UnUtils.pas',
  UnDMConexao in 'DataModule\UnDMConexao.pas' {DMConexao: TDataModule},
  UnInterfaces in 'Interfaces\UnInterfaces.pas',
  UnIRetornaTokenLogin in 'Interfaces\UnIRetornaTokenLogin.pas',
  UnIGetDadosComToken in 'Interfaces\UnIGetDadosComToken.pas',
  UnFrmBuscarHeranca in 'Heran�a\UnFrmBuscarHeranca.pas' {FrmBuscarHeranca},
  UnFrmBuscarDisciplina in 'UnFrmBuscarDisciplina.pas' {FrmBuscarDisciplina},
  UnFrmBuscarCurso in 'UnFrmBuscarCurso.pas' {FrmBuscarCurso},
  UnFrmCadastroUsuario in 'UnFrmCadastroUsuario.pas' {FrmCadastroUsuario},
  UnIPostDadosComToken in 'Interfaces\UnIPostDadosComToken.pas',
  UnFrmCadastroHeranca in 'Heran�a\UnFrmCadastroHeranca.pas' {FrmCadastroHeranca},
  UnFrmCadastroCurso in 'UnFrmCadastroCurso.pas' {FrmCadastroCurso},
  UnFrmCadastroDisciplina in 'UnFrmCadastroDisciplina.pas' {FrmCadastroDisciplina};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmMenuPrincipal, FrmMenuPrincipal);
  Application.CreateForm(TDMConexao, DMConexao);
  Application.CreateForm(TFrmLogin, FrmLogin);
  try
    vLogado := False;

    FrmLogin.ShowModal;
  finally
    FrmLogin.Free;
  end;

  Application.Run;
end.
