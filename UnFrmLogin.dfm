object FrmLogin: TFrmLogin
  Left = 0
  Top = 0
  Caption = 'Login'
  ClientHeight = 184
  ClientWidth = 223
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbUsuario: TLabel
    Left = 16
    Top = 24
    Width = 40
    Height = 13
    Caption = 'Usu'#225'rio:'
  end
  object lbSenha: TLabel
    Left = 16
    Top = 70
    Width = 30
    Height = 13
    Caption = 'Senha'
  end
  object btnLogar: TButton
    Left = 16
    Top = 131
    Width = 73
    Height = 25
    Caption = 'Logar'
    TabOrder = 2
    OnClick = btnLogarClick
  end
  object btnCancel: TButton
    Left = 128
    Top = 131
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = btnCancelClick
  end
  object edtUsuario: TEdit
    Left = 16
    Top = 43
    Width = 187
    Height = 21
    TabOrder = 0
  end
  object edtSenha: TMaskEdit
    Left = 16
    Top = 89
    Width = 187
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
    Text = ''
  end
end
