inherited FrmBuscarDisciplina: TFrmBuscarDisciplina
  Caption = 'Buscar Dados Disciplina'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    ExplicitWidth = 467
    inherited btnSair: TButton
      ExplicitLeft = 359
    end
  end
  inherited Panel2: TPanel
    ExplicitWidth = 467
    inherited dbGrid: TDBGrid
      Columns = <
        item
          Expanded = False
          FieldName = 'ID'
          Title.Caption = 'C'#243'digo'
          Width = 54
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEDISCIPLINA'
          Title.Caption = 'Disciplina'
          Width = 308
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'HORASAULA'
          Title.Caption = 'Horas Aula'
          Width = 64
          Visible = True
        end>
    end
  end
  inherited cdsGrid: TClientDataSet
    object cdsGridID: TWideStringField
      FieldName = 'ID'
      Size = 0
    end
    object cdsGridNOMEDISCIPLINA: TWideStringField
      FieldName = 'NOMEDISCIPLINA'
      Size = 0
    end
    object cdsGridHORASAULA: TWideStringField
      FieldName = 'HORASAULA'
      Size = 0
    end
  end
end
