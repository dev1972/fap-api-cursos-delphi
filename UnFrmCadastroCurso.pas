unit UnFrmCadastroCurso;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UnFrmCadastroHeranca, Vcl.StdCtrls,
  Vcl.ExtCtrls, System.JSON;

type
  TFrmCadastroCurso = class(TFrmCadastroHeranca)
    edtID: TEdit;
    edtNomeCurso: TEdit;
    lbNomeCurso: TLabel;
    lbID: TLabel;
    procedure btnSalvarClick(Sender: TObject);
  private
    { Private declarations }
  protected
    function MontaJSON: String; override;
  public
    { Public declarations }
  end;

var
  FrmCadastroCurso: TFrmCadastroCurso;

implementation

{$R *.dfm}

procedure TFrmCadastroCurso.btnSalvarClick(Sender: TObject);
begin
  if StrToIntDef(edtID.Text, 0) <= 0 then
  begin
    ShowMessage('C�digo Inv�lido... Verifique!')
  end;

  if Trim(edtNomeCurso.Text) = EmptyStr then
  begin
    ShowMessage('Nome Inv�lido... Verifique!')
  end;

  inherited;

  Self.Close;

end;

function TFrmCadastroCurso.MontaJSON: String;
var
  FcJSONObject: TJSONObject;
begin
  inherited;
  Result        := EmptyStr;
  FcJSONObject  := TJSONObject.Create;
  try
    FcJSONObject.AddPair('id', TJSONNumber.Create(StrToInt(edtID.Text)));
    FcJSONObject.AddPair('nomeCurso', edtNomeCurso.Text);
    Result := FcJSONObject.ToString;
  finally
    FcJSONObject.Free;
  end;
end;

end.
