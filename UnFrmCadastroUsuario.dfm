object FrmCadastroUsuario: TFrmCadastroUsuario
  Left = 0
  Top = 0
  Caption = 'Cadastro de Usuarios'
  ClientHeight = 207
  ClientWidth = 448
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbUsuario: TLabel
    Left = 24
    Top = 80
    Width = 40
    Height = 13
    Caption = 'Usu'#225'rio:'
  end
  object Label1: TLabel
    Left = 24
    Top = 126
    Width = 30
    Height = 13
    Caption = 'Senha'
  end
  object Label2: TLabel
    Left = 264
    Top = 80
    Width = 25
    Height = 13
    Caption = 'Role:'
  end
  object Label3: TLabel
    Left = 24
    Top = 34
    Width = 37
    Height = 13
    Caption = 'C'#243'digo:'
  end
  object DBNavigator1: TDBNavigator
    Left = 0
    Top = 0
    Width = 448
    Height = 25
    DataSource = dsUsuario
    VisibleButtons = [nbPrior, nbNext, nbInsert, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]
    Align = alTop
    TabOrder = 0
    ExplicitLeft = 160
    ExplicitTop = 8
    ExplicitWidth = 240
  end
  object DBEdit1: TDBEdit
    Left = 24
    Top = 99
    Width = 193
    Height = 21
    DataField = 'login'
    DataSource = dsUsuario
    TabOrder = 2
  end
  object DBEdit2: TDBEdit
    Left = 24
    Top = 145
    Width = 193
    Height = 21
    DataField = 'senha'
    DataSource = dsUsuario
    TabOrder = 3
  end
  object DBComboBox1: TDBComboBox
    Left = 264
    Top = 99
    Width = 153
    Height = 21
    DataField = 'role'
    DataSource = dsUsuario
    Items.Strings = (
      'User'
      'DBA'
      'Admin')
    TabOrder = 4
  end
  object DBEdit3: TDBEdit
    Left = 24
    Top = 53
    Width = 65
    Height = 21
    DataField = 'id'
    DataSource = dsUsuario
    TabOrder = 1
  end
  object qUsuario: TFDQuery
    Connection = DMConexao.FDConexao
    SQL.Strings = (
      'SELECT ID,'
      '       LOGIN,'
      '       SENHA,'
      '       ROLE'
      '  FROM USUARIO')
    Left = 384
    Top = 152
    object qUsuarioid: TIntegerField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object qUsuariologin: TWideStringField
      FieldName = 'login'
      Origin = '"login"'
      Size = 8190
    end
    object qUsuariosenha: TWideStringField
      FieldName = 'senha'
      Origin = 'senha'
      Size = 8190
    end
    object qUsuariorole: TWideStringField
      FieldName = 'role'
      Origin = '"role"'
      Size = 8190
    end
  end
  object dsUsuario: TDataSource
    AutoEdit = False
    DataSet = qUsuario
    Left = 336
    Top = 152
  end
end
