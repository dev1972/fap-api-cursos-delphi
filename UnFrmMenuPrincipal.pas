unit UnFrmMenuPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Menus,
  Vcl.Imaging.pngimage, Vcl.ComCtrls;

type
  TFrmMenuPrincipal = class(TForm)
    MainMenu: TMainMenu;
    mSair: TMenuItem;
    miTrocarUsuario: TMenuItem;
    miSair: TMenuItem;
    mCurso: TMenuItem;
    mDisciplina: TMenuItem;
    miBuscarCurso: TMenuItem;
    miBuscarDisciplina: TMenuItem;
    mUsuario: TMenuItem;
    miCadastrarUsuarioBD: TMenuItem;
    Image1: TImage;
    StatusBar1: TStatusBar;
    miCadastrarCurso: TMenuItem;
    miCadastrarDisciplina: TMenuItem;
    procedure miSairClick(Sender: TObject);
    procedure miTrocarUsuarioClick(Sender: TObject);
    procedure miBuscarCursoClick(Sender: TObject);
    procedure miBuscarDisciplinaClick(Sender: TObject);
    procedure miCadastrarUsuarioBDClick(Sender: TObject);
    procedure miCadastrarCursoClick(Sender: TObject);
    procedure miCadastrarDisciplinaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmMenuPrincipal: TFrmMenuPrincipal;

implementation

uses
  UnUtils, UnFrmLogin, unFrmBuscarCurso, UnFrmBuscarDisciplina,
  UnFrmCadastroUsuario, UnFrmCadastroCurso, UnFrmCadastroDisciplina;

{$R *.dfm}

procedure TFrmMenuPrincipal.miTrocarUsuarioClick(Sender: TObject);
begin
  Application.CreateForm(TFrmLogin, FrmLogin);
  try
    FrmLogin.ShowModal;
  finally
    FrmLogin.Free;
  end;
end;

procedure TFrmMenuPrincipal.miBuscarCursoClick(Sender: TObject);
begin
  Application.CreateForm(TFrmBuscarCurso, FrmBuscarCurso);
  try
    FrmBuscarCurso.cURL_Request := URL_CURSO;
    FrmBuscarCurso.ShowModal;
  finally
    FrmBuscarCurso.Free;
  end;
end;

procedure TFrmMenuPrincipal.miBuscarDisciplinaClick(Sender: TObject);
begin
  Application.CreateForm(TFrmBuscarDisciplina, FrmBuscarDisciplina);
  try
    FrmBuscarDisciplina.cURL_Request := URL_DISCIPLINA;
    FrmBuscarDisciplina.ShowModal;
  finally
    FrmBuscarDisciplina.Free;
  end;
end;

procedure TFrmMenuPrincipal.miCadastrarCursoClick(Sender: TObject);
begin
  Application.CreateForm(TFrmCadastroCurso, FrmCadastroCurso);
  try
    FrmCadastroCurso.cURL_Request := URL_CURSO;
    FrmCadastroCurso.ShowModal;
  finally
    FrmCadastroCurso.Free;
  end;
end;

procedure TFrmMenuPrincipal.miCadastrarDisciplinaClick(Sender: TObject);
begin
  Application.CreateForm(TFrmCadastroDisciplina, FrmCadastroDisciplina);
  try
    FrmCadastroDisciplina.cURL_Request := URL_DISCIPLINA;
    FrmCadastroDisciplina.ShowModal;
  finally
    FrmCadastroDisciplina.Free;
  end;
end;

procedure TFrmMenuPrincipal.miCadastrarUsuarioBDClick(Sender: TObject);
begin
  Application.CreateForm(TFrmCadastroUsuario, FrmCadastroUsuario);
  try
    FrmCadastroUsuario.ShowModal;
  finally
    FrmCadastroUsuario.Free;
  end;
end;

procedure TFrmMenuPrincipal.miSairClick(Sender: TObject);
begin
  Self.Close;
end;

end.
