inherited FrmCadastroDisciplina: TFrmCadastroDisciplina
  Caption = 'Cadastro de Disciplinas'
  ClientHeight = 252
  ClientWidth = 304
  ExplicitWidth = 320
  ExplicitHeight = 291
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 304
    ExplicitWidth = 501
    inherited btnSair: TButton
      Left = 196
      ExplicitLeft = 393
    end
  end
  inherited Panel2: TPanel
    Width = 304
    Height = 195
    ExplicitLeft = 0
    ExplicitTop = 57
    ExplicitWidth = 501
    ExplicitHeight = 295
    object lbNomeDisciplina: TLabel
      Left = 24
      Top = 77
      Width = 92
      Height = 13
      Caption = 'Nome da Disciplina:'
    end
    object lbID: TLabel
      Left = 24
      Top = 22
      Width = 37
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object lbHorasAula: TLabel
      Left = 24
      Top = 134
      Width = 56
      Height = 13
      Caption = 'Horas Aula:'
    end
    object edtID: TEdit
      Left = 24
      Top = 40
      Width = 41
      Height = 21
      NumbersOnly = True
      TabOrder = 0
      Text = '0'
    end
    object edtNomeDisciplina: TEdit
      Left = 24
      Top = 96
      Width = 249
      Height = 21
      TabOrder = 1
    end
    object edtHorasAula: TEdit
      Left = 24
      Top = 152
      Width = 77
      Height = 21
      NumbersOnly = True
      TabOrder = 2
      Text = '0'
    end
  end
end
