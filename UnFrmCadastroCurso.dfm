inherited FrmCadastroCurso: TFrmCadastroCurso
  Caption = 'Cadastro de Curso'
  ClientHeight = 249
  ClientWidth = 287
  ExplicitWidth = 303
  ExplicitHeight = 288
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 287
    ExplicitWidth = 501
    inherited btnSalvar: TButton
      Width = 88
      ExplicitWidth = 88
    end
    inherited btnSair: TButton
      Left = 198
      Width = 88
      OnClick = nil
      ExplicitLeft = 200
      ExplicitWidth = 88
    end
  end
  inherited Panel2: TPanel
    Width = 287
    Height = 192
    ExplicitLeft = 0
    ExplicitTop = 57
    ExplicitWidth = 501
    ExplicitHeight = 295
    object lbNomeCurso: TLabel
      Left = 24
      Top = 77
      Width = 77
      Height = 13
      Caption = 'Nome do Curso:'
    end
    object lbID: TLabel
      Left = 24
      Top = 22
      Width = 37
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object edtID: TEdit
      Left = 24
      Top = 40
      Width = 41
      Height = 21
      NumbersOnly = True
      TabOrder = 0
      Text = '0'
    end
    object edtNomeCurso: TEdit
      Left = 24
      Top = 96
      Width = 249
      Height = 21
      TabOrder = 1
    end
  end
end
