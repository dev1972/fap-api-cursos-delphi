unit UnFrmLogin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Mask, Vcl.StdCtrls, Data.DB, FireDAC.Stan.Param;

type
  TFrmLogin = class(TForm)
    btnLogar: TButton;
    btnCancel: TButton;
    edtUsuario: TEdit;
    lbUsuario: TLabel;
    lbSenha: TLabel;
    edtSenha: TMaskEdit;
    procedure btnLogarClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FTentativas: Integer;
    procedure TerminateApplication(ALogado: Boolean);
  public
    { Public declarations }
  end;

var
  FrmLogin: TFrmLogin;

implementation

uses
  UnUtils, UnDMConexao;

{$R *.dfm}

procedure TFrmLogin.btnCancelClick(Sender: TObject);
begin
  TerminateApplication(vLogado);
  Self.Close;
end;

procedure TFrmLogin.btnLogarClick(Sender: TObject);
const
  TRES = 3;
begin
  DMConexao.qLogin.Close;
  DMConexao.qLogin.ParamByName('LOGIN').AsString := LowerCase(edtUsuario.Text);
  DMConexao.qLogin.ParamByName('SENHA').AsString := edtSenha.Text;
  DMConexao.qLogin.Open;

  if DMConexao.qLogin.IsEmpty then
  begin
    Application.MessageBox('Usu�rio n�o encontrado! Verifique...','Aten��o', MB_ICONERROR);
    vLogado := False;
    Inc(FTentativas);
    Abort;
  end
  else
  begin
    vLogado := True;
  end;

  if (FTentativas = TRES) and (not vLogado) then
  begin
    TerminateApplication(vLogado);
  end;

  if vLogado then
  begin
    Self.Close;
  end;
end;

procedure TFrmLogin.FormCreate(Sender: TObject);
begin
  FTentativas := 0;
end;

procedure TFrmLogin.TerminateApplication(ALogado: Boolean);
begin
  if not ALogado then
  begin
    Application.Terminate;
  end;
end;

end.
