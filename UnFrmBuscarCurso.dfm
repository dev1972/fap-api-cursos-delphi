inherited FrmBuscarCurso: TFrmBuscarCurso
  Caption = 'Buscar Dados Curso'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    ExplicitWidth = 467
    inherited btnSair: TButton
      ExplicitLeft = 359
    end
  end
  inherited Panel2: TPanel
    ExplicitWidth = 467
    inherited dbGrid: TDBGrid
      Columns = <
        item
          Expanded = False
          FieldName = 'ID'
          Title.Caption = 'C'#243'digo'
          Width = 69
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECURSO'
          Title.Caption = 'Nome Curso'
          Width = 356
          Visible = True
        end>
    end
  end
  inherited cdsGrid: TClientDataSet
    object cdsGridID: TWideStringField
      FieldName = 'ID'
      Size = 0
    end
    object cdsGridNOMECURSO: TWideStringField
      FieldName = 'NOMECURSO'
      Size = 0
    end
  end
end
