unit UnDMConexao;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.PG,
  FireDAC.Phys.PGDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, System.StrUtils;

type
  TDMConexao = class(TDataModule)
    FDConexao: TFDConnection;
    qLogin: TFDQuery;
    dsLogin: TDataSource;
    qLoginid: TIntegerField;
    qLoginlogin: TWideStringField;
    qLoginsenha: TWideStringField;
    qLoginrole: TWideStringField;
    procedure qLoginAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMConexao: TDMConexao;

implementation

uses
  UnUtils, UnFrmMenuPrincipal;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDMConexao.qLoginAfterOpen(DataSet: TDataSet);
const
  USUARIO = 0;
  ROLE    = 1;
begin
  FrmMenuPrincipal.StatusBar1.Panels[USUARIO].Text := Concat(USU_LOGADO, IfThen(qLogin.IsEmpty, NENHUM, qLoginlogin.AsString));
  FrmMenuPrincipal.StatusBar1.Panels[ROLE].Text    := Concat(ROLE_LOGADO, IfThen(qLogin.IsEmpty, NENHUM, qLoginrole.AsString));
end;

end.
