object DMConexao: TDMConexao
  OldCreateOrder = False
  Height = 160
  Width = 306
  object FDConexao: TFDConnection
    Params.Strings = (
      'Server=ec2-54-146-84-101.compute-1.amazonaws.com'
      'User_Name=egjsydrnpkluzi'
      
        'Password=20b57c8c0fe2e3de226a15474a3bd0347787c2833190108eada4f8f' +
        '01a765149'
      'Database=d9qhdosp0k7rci'
      'PGAdvanced=sslmode=require'
      'DriverID=PG')
    FetchOptions.AssignedValues = [evLiveWindowParanoic]
    ResourceOptions.AssignedValues = [rvSilentMode]
    Left = 32
    Top = 8
  end
  object qLogin: TFDQuery
    AfterOpen = qLoginAfterOpen
    Connection = FDConexao
    SQL.Strings = (
      'SELECT ID,'
      '       LOGIN,'
      '       SENHA,'
      '       ROLE'
      '  FROM USUARIO'
      ' WHERE LOWER(LOGIN) = LOWER(:LOGIN)'
      '   AND SENHA = :SENHA ')
    Left = 248
    Top = 8
    ParamData = <
      item
        Name = 'LOGIN'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'SENHA'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
    object qLoginid: TIntegerField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object qLoginlogin: TWideStringField
      FieldName = 'login'
      Origin = '"login"'
      Size = 8190
    end
    object qLoginsenha: TWideStringField
      FieldName = 'senha'
      Origin = 'senha'
      Size = 8190
    end
    object qLoginrole: TWideStringField
      FieldName = 'role'
      Origin = '"role"'
      Size = 8190
    end
  end
  object dsLogin: TDataSource
    DataSet = qLogin
    Left = 248
    Top = 64
  end
end
